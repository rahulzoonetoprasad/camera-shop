<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Validator;
class ListController extends Controller
{
    /**
     * Fetch categories list
     * @method fetchCategory
     * @param null
     */
    public function fetchCategory(Request $request)
    {
        try{
            $data=Category::all();
            $resopnse['status']= 200;         
            $resopnse['message'] = 'success';
            $resopnse['data'] = $data;
            return response()->json($resopnse);

        }catch(\Exception $e){
            
             $resopnse['status']= 401;         
             $resopnse['message'] = $request->filled('debug')?$e->getMessage():'Something went wrong';
             return response()->json($resopnse);
         }
    }
    /**
     * get all product list
     * @method allProduct
     * @pram null
     */
    public function allProduct()
    {
        
        try{
        $data=Product::with('category')->get();
        $resopnse['status']= 200;         
        $resopnse['message'] = 'success';
        $resopnse['data'] = $data;
        return response()->json($resopnse);
        }catch(\Exception $e){
                
            $resopnse['status']= 401;         
            $resopnse['message'] = $request->filled('debug')?$e->getMessage():'Something went wrong';
            return response()->json($resopnse);
        }
    }
    /**
     * all product of specific category
     * @method ProductByCategory
     * @param category_id
     */
    public function ProductByCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id'=>'required',
            ]); 
            if ($validator->fails()){
                return response()->json([$validator->errors()]);
            }else{
        try{
            $data=Product::with('category')->where('cat_id',$request->category_id)->get();
            $resopnse['status']= 200;         
            $resopnse['message'] = 'success';
            $resopnse['data'] = $data;
            return response()->json($resopnse);
            
    }catch(\Exception $e){
            
        $resopnse['status']= 401;         
        $resopnse['message'] = $request->filled('debug')?$e->getMessage():'Something went wrong';
        return response()->json($resopnse);
    }
    }
}

}
