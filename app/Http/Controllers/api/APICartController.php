<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use App\User;
use App\Product;
use App\Cart;
class APICartController extends Controller
{
    /**
     * Fetch Cart data of specific user
     * @method index
     * @param null
     */
    public function index(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['message'=>'user not found','status'=>404], 404);
        }
         if($user->id!=$request->user_id){
            return response()->json(['message'=>'Token mismatch','status'=>201],201);
         }
         $cart=Cart::where('user_id',$request->user_id)->first();
         
         $details = !empty($cart)?unserialize($cart->details):'';
         return response()->json(['message'=>'success','status'=>200,'data'=>$details],200);

    }
    /**
     * Add Product to cart
     * @method addToCart
     * @param null
     */
    public function addToCart(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'product_id'=>'required',
                'user_id'=>'required',
                'quantity'=>'required',
            ]); 
            if ($validator->fails()){
                return response()->json([$validator->errors()]);
            }else{
                try{
                if(!$user=User::find($request->user_id)){
                    $resopnse['status']= 201;         
                    $resopnse['message'] = 'user not exist';
                    return response()->json($resopnse);
                }
                if(!$product=Product::find($request->product_id)){
                    $resopnse['status']= 201;         
                    $resopnse['message'] = 'product not exist';
                    return response()->json($resopnse);
                }
                $cartdata=array();
                $details = array(
                'products' => array(array(
                    'id' => $product->id,
                    'name' => $product->name,
                    'quantity' => $request->quantity,
                    'price' =>($product->price * $request->quantity)
                )),
            );
            
            $cart=Cart::where('user_id',$request->user_id)->first();
            if(!empty($cart)){
            $added_products= unserialize($cart->details);

             $ids=array_column($added_products['products'],'id');
             
            if (in_array($request->product_id, $ids)){
                $resopnse['status']= 200;         
                $resopnse['message'] = 'Already added';
                $resopnse['data'] = '';
                return response()->json($resopnse); 
            }else{
                
                array_push($added_products['products'],$details['products'][0]);
                
            }}else{
                $added_products=$details;
            }
            $cartdata['details'] =serialize($added_products);
            $cartdata['user_id'] =$user->id;
            Cart::updateOrCreate(['user_id'=>$user->id],$cartdata);
            $resopnse['status']= 200;         
            $resopnse['message'] = 'success';
            $resopnse['data'] = $added_products;
            return response()->json($resopnse);
        }catch(\Exception $e){
            
            $resopnse['status']= 401;         
            $resopnse['message'] = $request->filled('debug')?$e->getMessage():'Something went wrong';
            return response()->json($resopnse);
        }
        }
    }
}
