<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use App\User;
class APILoginController extends Controller
{
    /**
     * User Login 
     * @method login
     * @param null
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'password'=>'required',
            ]); 
            if ($validator->fails()){
                return response()->json([$validator->errors()]);
            }else{
                $credentials = $request->only('password','email');
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials','status'=>401], 200);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token','status'=>401], 500);
            }
         $user = auth()->user(); 
         $data['token']=$token;
         $data['id']=$user->id;
         $data['name']=$user->name;
         
         return response()->json([
            'message' => 'Success',
            'data' => $data,
            'status'=>200
        ]);
        }
    }
}
