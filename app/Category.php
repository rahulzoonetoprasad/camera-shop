<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'type','model','created_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
