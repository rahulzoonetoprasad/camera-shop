<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([

    
    'namespace' => 'api',
    'prefix' => 'auth'

], function () {
    Route::post('login', 'APILoginController@login');
    Route::get('category/list', 'ListController@fetchCategory');
    Route::get('all/products','ListController@allProduct');
    Route::get('category/product','ListController@ProductByCategory');

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::post('cart/data/list','APICartController@index');
        Route::Post('cart/add','APICartController@addToCart');
    });


});