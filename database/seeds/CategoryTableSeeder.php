<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $faker = \Faker\Factory::create();
        //print_r($faker);exit;
        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 20; $i++) {
            Category::create([
                'name' => $faker->company,
                'type' => $faker->sentence,
                'model' => $faker->year,
            ]);
        }
    }
}
